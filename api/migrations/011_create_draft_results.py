steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE draft_results(
            draft_id INT REFERENCES drafts(draft_id),
            user_team_id INT REFERENCES user_teams(user_team_id),
            player_id INT REFERENCES players(player_id),
            pick_num INT
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE draft_results;
        """,
    ],
]
