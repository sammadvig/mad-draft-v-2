steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE drafts(
            draft_id SERIAL PRIMARY KEY,
            season_id INT REFERENCES seasons(season_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE drafts;
        """,
    ],
]
