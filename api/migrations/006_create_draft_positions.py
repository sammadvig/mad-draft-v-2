steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE draft_positions(
            user_team_id INT REFERENCES user_teams(user_team_id),
            draft_id INT REFERENCES drafts(draft_id),
            pick_number INT
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE draft_positions;
        """,
    ],
]
