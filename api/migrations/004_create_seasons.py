steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE seasons(
            season_id SERIAL PRIMARY KEY,
            league_id INT REFERENCES leagues(league_id),
            year INT
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE seasons;
        """,
    ],
]
