steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE school_teams(
            id INT PRIMARY KEY,
            name VARCHAR(64) NULL,
            abbrev VARCHAR(64) NULL,
            active VARCHAR(4) NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE school_teams;
        """,
    ],
]
