steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE db_last_updated (
            db_name VARCHAR(32) PRIMARY KEY,
            last_updated TIMESTAMPTZ
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE db_last_updated;
        """,
    ],
]
