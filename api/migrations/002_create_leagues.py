steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE leagues(
            league_id SERIAL PRIMARY KEY,
            league_name VARCHAR(64),
            created TIMESTAMP NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE leagues;
        """,
    ],
]
