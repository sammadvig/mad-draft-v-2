steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE watchlist_players(
            watchlist_id INT REFERENCES watchlists(watchlist_id),
            player_id INT REFERENCES tourney_player_details(player_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE watchlist_players;
        """,
    ],
]
