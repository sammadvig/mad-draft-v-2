steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE player_stats(
            player_id INT PRIMARY KEY,
            games_played INT,
            games_started INT,
            minutes_pg NUMERIC(4,1),
            points_pg NUMERIC(4,1),
            fg_made_pg NUMERIC(4,1),
            fg_attempts_pg NUMERIC(4,1),
            three_pt_made_pg NUMERIC(4,1),
            three_pt_attempts_pg NUMERIC(4,1),
            free_throws_made_pg NUMERIC(4,1),
            free_throw_attempts_pg NUMERIC(4,1),
            rebounds_pg NUMERIC(4,1),
            assists_pg NUMERIC(4,1),
            steals_pg NUMERIC(4,1),
            blocks_pg NUMERIC(4,1),
            offensive_reb_pg NUMERIC(4,1),
            turnovers_pg NUMERIC(4,1),
            personal_fouls_pg NUMERIC(4,1)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE player_stats;
        """,
    ],
]
