steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE player_performances(
            id INT PRIMARY KEY,
            player_name VARCHAR(64) NULL,
            player_id INT NULL,
            team_id INT NULL,
            starter VARCHAR(4) NULL,
            league VARCHAR(32) NULL,
            minutes SMALLINT NULL,
            points SMALLINT NULL,
            fg_made SMALLINT NULL,
            fg_attempts SMALLINT NULL,
            three_pt_made SMALLINT NULL,
            three_pt_attempts SMALLINT NULL,
            free_throws_made SMALLINT NULL,
            free_throw_attempts SMALLINT NULL,
            rebounds SMALLINT NULL,
            assists SMALLINT NULL,
            steals SMALLINT NULL,
            blocks SMALLINT NULL,
            offensive_reb SMALLINT NULL,
            turnovers SMALLINT NULL,
            personal_fouls SMALLINT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE player_performances;
        """,
    ],
]
