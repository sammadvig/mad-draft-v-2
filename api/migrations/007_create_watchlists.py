steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE watchlists(
            watchlist_id SERIAL PRIMARY KEY,
            user_team_id INT REFERENCES user_teams(user_team_id),
            draft_id INT REFERENCES drafts(draft_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE watchlists;
        """,
    ],
]
