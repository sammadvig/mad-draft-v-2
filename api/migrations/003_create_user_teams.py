steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE user_teams(
            user_team_id SERIAL PRIMARY KEY,
            user_id INT REFERENCES accounts(user_id),
            user_team_name VARCHAR(32),
            league_id INT REFERENCES leagues(league_id),
            created TIMESTAMP NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE user_teams;
        """,
    ],
]
