steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE players(
            player_id SERIAL PRIMARY KEY,
            full_name VARCHAR(64) NULL,
            school_id INT NULL,
            position VARCHAR(8) NULL,
            height_in_inches INT NULL,
            class VARCHAR(8) NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE players;
        """,
    ],
]
