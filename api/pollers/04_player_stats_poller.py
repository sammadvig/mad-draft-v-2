import psycopg
import os

pg_db = os.environ["POSTGRES_DB"]
db_host = os.environ["DB_HOST"]
db_user = os.environ["DB_USER"]
db_pass = os.environ["DB_PASS"]
db_port = os.environ["DB_PORT"]
natstat_key = os.environ["NATSTAT_KEY"]

conn = psycopg.connect(
    dbname=pg_db,
    host=db_host,
    user=db_user,
    password=db_pass,
    port=db_port,
)
db = conn.cursor()

db.execute(
    """
    INSERT INTO player_stats(
        player_id,
        games_played,
        games_started,
        minutes_pg,
        points_pg,
        fg_made_pg,
        fg_attempts_pg,
        three_pt_made_pg,
        three_pt_attempts_pg,
        free_throws_made_pg,
        free_throw_attempts_pg,
        rebounds_pg,
        assists_pg,
        steals_pg,
        blocks_pg,
        offensive_reb_pg,
        turnovers_pg,
        personal_fouls_pg
    )
    SELECT
        player_id,
        COUNT(*) as games_played,
        SUM(CASE WHEN starter = 'Y' THEN 1 ELSE 0 END) as games_started,
        AVG(minutes) as minutes_pg,
        AVG(points) as points_pg,
        AVG(fg_made) as fg_made_pg,
        AVG(fg_attempts) as fg_attempts_pg,
        AVG(three_pt_made) as three_pt_made_pg,
        AVG(three_pt_attempts) as three_pt_attempts_pg,
        AVG(free_throws_made) as free_throws_made_pg,
        AVG(free_throw_attempts) as free_throw_attempts_pg,
        AVG(rebounds) as rebounds_pg,
        AVG(assists) as assists_pg,
        AVG(steals) as steals_pg,
        AVG(blocks) as blocks_pg,
        AVG(offensive_reb) as offensive_reb_pg,
        AVG(turnovers) as turnovers_pg,
        AVG(personal_fouls) as personal_fouls_pg
    FROM player_performances
    WHERE player_id != 0
    GROUP BY player_id
    ON CONFLICT (player_id)
    DO UPDATE SET
    games_played = excluded.games_played,
    games_started = excluded.games_started,
    minutes_pg = excluded.minutes_pg,
    points_pg = excluded.points_pg,
    fg_made_pg = excluded.fg_made_pg,
    fg_attempts_pg = excluded.fg_attempts_pg,
    three_pt_made_pg = excluded.three_pt_made_pg,
    three_pt_attempts_pg = excluded.three_pt_attempts_pg,
    free_throws_made_pg = excluded.free_throws_made_pg,
    free_throw_attempts_pg = excluded.free_throw_attempts_pg,
    rebounds_pg = excluded.rebounds_pg,
    assists_pg = excluded.assists_pg,
    steals_pg = excluded.steals_pg,
    blocks_pg = excluded.blocks_pg,
    offensive_reb_pg = excluded.offensive_reb_pg,
    turnovers_pg = excluded.turnovers_pg,
    personal_fouls_pg = excluded.personal_fouls_pg;
    """
)

conn.commit()
db.close()
conn.close()
