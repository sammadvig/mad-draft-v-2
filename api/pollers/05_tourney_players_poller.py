import psycopg
import os

from references.tourney_teams import TEAMS_IN_TOURNEY

pg_db = os.environ["POSTGRES_DB"]
db_host = os.environ["DB_HOST"]
db_user = os.environ["DB_USER"]
db_pass = os.environ["DB_PASS"]
db_port = os.environ["DB_PORT"]
natstat_key = os.environ["NATSTAT_KEY"]

conn = psycopg.connect(
    dbname=pg_db,
    host=db_host,
    user=db_user,
    password=db_pass,
    port=db_port,
)
db = conn.cursor()

db.execute(
    """
    INSERT INTO tourney_player_details(
        player_id,
        player_name,
        school_id,
        school_name,
        school_abbrev,
        games_played,
        games_started,
        minutes_pg,
        points_pg,
        fg_made_pg,
        fg_attempts_pg,
        three_pt_made_pg,
        three_pt_attempts_pg,
        free_throws_made_pg,
        free_throw_attempts_pg,
        rebounds_pg,
        assists_pg,
        steals_pg,
        blocks_pg,
        offensive_reb_pg,
        turnovers_pg,
        personal_fouls_pg
    )
    SELECT
        players.player_id,
        players.full_name,
        schools.id,
        schools.name,
        schools.abbrev,
        ps.games_played,
        ps.games_started,
        ps.minutes_pg,
        ps.points_pg,
        ps.fg_made_pg,
        ps.fg_attempts_pg,
        ps.three_pt_made_pg,
        ps.three_pt_attempts_pg,
        ps.free_throws_made_pg,
        ps.free_throw_attempts_pg,
        ps.rebounds_pg,
        ps.assists_pg,
        ps.steals_pg,
        ps.blocks_pg,
        ps.offensive_reb_pg,
        ps.turnovers_pg,
        ps.personal_fouls_pg
    FROM
    player_stats ps
    INNER JOIN players on ps.player_id = players.player_id
    LEFT JOIN school_teams schools
    ON players.school_id = schools.id
    WHERE schools.id in (
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s,
        %s, %s, %s, %s
        )
    ON CONFLICT (player_id)
    DO UPDATE SET
        player_id = excluded.player_id,
        player_name = excluded.player_name,
        school_id = excluded.school_id,
        school_name = excluded.school_name,
        school_abbrev = excluded.school_abbrev,
        games_played = excluded.games_played,
        games_started = excluded.games_started,
        minutes_pg = excluded.minutes_pg,
        points_pg = excluded.points_pg,
        fg_made_pg = excluded.fg_made_pg,
        fg_attempts_pg = excluded.fg_attempts_pg,
        three_pt_made_pg = excluded.three_pt_made_pg,
        three_pt_attempts_pg = excluded.three_pt_attempts_pg,
        free_throws_made_pg = excluded.free_throws_made_pg,
        free_throw_attempts_pg = excluded.free_throw_attempts_pg,
        rebounds_pg = excluded.rebounds_pg,
        assists_pg = excluded.assists_pg,
        steals_pg = excluded.steals_pg,
        blocks_pg = excluded.blocks_pg,
        offensive_reb_pg = excluded.offensive_reb_pg,
        turnovers_pg = excluded.turnovers_pg,
        personal_fouls_pg = excluded.personal_fouls_pg
    """,
    TEAMS_IN_TOURNEY,
)
conn.commit()
db.close()
conn.close()
