import json
import psycopg
import requests
import os

from references.convert_last_updated import convert_last_updated
from references.insert_player_perfs import insert_player_perfs
from references.last_updated import update_last_updated

pg_db = os.environ["POSTGRES_DB"]
db_host = os.environ["DB_HOST"]
db_user = os.environ["DB_USER"]
db_pass = os.environ["DB_PASS"]
db_port = os.environ["DB_PORT"]
natstat_key = os.environ["NATSTAT_KEY"]

conn = psycopg.connect(
    dbname=pg_db,
    host=db_host,
    user=db_user,
    password=db_pass,
    port=db_port,
)
db = conn.cursor()
last_updated_date = convert_last_updated(db, "player_performances")
if last_updated_date is None:
    initial_fetch_url = (
        f"https://api.natstat.com/v2/playerperfs/MBB/?"
        f"key={natstat_key}&format=json&season=2024&max=1000"
    )
else:
    initial_fetch_url = (
        f"https://api.natstat.com/v2/playerperfs/MBB/?"
        f"key={natstat_key}&format=json&season=2024&max=1000"
        f"&start={last_updated_date}"
    )
insert_player_perfs(initial_fetch_url, db)
update_last_updated(db, "player_performances")

conn.commit()
db.close()
conn.close()
