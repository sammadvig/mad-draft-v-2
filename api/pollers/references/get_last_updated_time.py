def get_last_updated_time(db, db_name: str):
    last_updated = db.execute(
        """
        SELECT last_updated
        FROM
        db_last_updated
        WHERE
        db_name = %s;
        """,
        [
            db_name,
        ],
    )
    updated_time = last_updated.fetchone()
    print(updated_time)
    if updated_time is None:
        print(db_name, "is none")
        return None
    else:
        print(db_name, "is not none")
        print("updated_time:", updated_time)
        print("subscript updated_time:", updated_time[0])
        return updated_time[0]
