def update_last_updated(db, db_name: str):
    db.execute(
        """
        INSERT INTO db_last_updated (db_name, last_updated)
        VALUES
        (%s, CURRENT_TIMESTAMP)
        ON CONFLICT (db_name)
        DO UPDATE SET last_updated = CURRENT_TIMESTAMP;
        """,
        [db_name],
    )
