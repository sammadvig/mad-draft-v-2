import requests
import json


def insert_player_perfs(fetch_url, db):
    res = requests.get(fetch_url)
    response = json.loads(res.text)
    if response["success"] == "1":
        performances = response["performances"]
        for performance in performances:
            details = performances[performance]
            performance_id = details.get("ID")
            player_name = details.get("Player")
            player_id = details.get("Player_ID")
            team_data = details.get("team")
            if team_data is not None:
                team_id = team_data.get("ID")
            else:
                team_id = None
            starter = details.get("Starter")
            league = details.get("League")
            minutes = details.get("MIN")
            points = details.get("PTS")
            fg_made = details.get("FGM")
            fg_attempts = details.get("FGA")
            three_pt_made = details.get("ThreeFM")
            three_pt_attempts = details.get("ThreeFA")
            free_throws_made = details.get("FTM")
            free_throw_attempts = details.get("FTA")
            rebounds = details.get("REB")
            assists = details.get("AST")
            steals = details.get("STL")
            blocks = details.get("BLK")
            offensive_reb = details.get("OREB")
            turnovers = details.get("TO")
            personal_fouls = details.get("PF")

            db.execute(
                """
                INSERT INTO player_performances(
                    id,
                    player_name,
                    player_id,
                    team_id,
                    starter,
                    league,
                    minutes,
                    points,
                    fg_made,
                    fg_attempts,
                    three_pt_made,
                    three_pt_attempts,
                    free_throws_made,
                    free_throw_attempts,
                    rebounds,
                    assists,
                    steals,
                    blocks,
                    offensive_reb,
                    turnovers,
                    personal_fouls
                )
                VALUES (
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s
                )
                ON CONFLICT (id)
                DO UPDATE SET
                    player_name = %s,
                    player_id = %s,
                    team_id = %s,
                    starter = %s,
                    league = %s,
                    minutes = %s,
                    points = %s,
                    fg_made = %s,
                    fg_attempts = %s,
                    three_pt_made = %s,
                    three_pt_attempts = %s,
                    free_throws_made = %s,
                    free_throw_attempts = %s,
                    rebounds = %s,
                    assists = %s,
                    steals = %s,
                    blocks = %s,
                    offensive_reb = %s,
                    turnovers = %s,
                    personal_fouls = %s
                """,
                [
                    performance_id,
                    player_name,
                    player_id,
                    team_id,
                    starter,
                    league,
                    minutes,
                    points,
                    fg_made,
                    fg_attempts,
                    three_pt_made,
                    three_pt_attempts,
                    free_throws_made,
                    free_throw_attempts,
                    rebounds,
                    assists,
                    steals,
                    blocks,
                    offensive_reb,
                    turnovers,
                    personal_fouls,
                    player_name,
                    player_id,
                    team_id,
                    starter,
                    league,
                    minutes,
                    points,
                    fg_made,
                    fg_attempts,
                    three_pt_made,
                    three_pt_attempts,
                    free_throws_made,
                    free_throw_attempts,
                    rebounds,
                    assists,
                    steals,
                    blocks,
                    offensive_reb,
                    turnovers,
                    personal_fouls,
                ],
            )
    metadata = response["meta"]
    current_page = metadata.get("Page")
    total_pages = metadata.get("Total_Pages")
    next_page_url = metadata.get("Next_Page")
    print("Completed Page:", current_page)
    if int(current_page) < int(total_pages):
        insert_player_perfs(next_page_url, db)
