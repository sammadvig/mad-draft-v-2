def add_zero(value: str):
    if len(value) == 1:
        value = "0" + value
    return value
