from references.get_last_updated_time import get_last_updated_time
from references.add_zero import add_zero


def convert_last_updated(db, db_name: str):
    last_updated = get_last_updated_time(db, db_name)

    if last_updated is None:
        return None
    last_updated_year = str(last_updated.year)
    last_updated_month = str(last_updated.month)
    last_updated_day = str(last_updated.day)

    last_updated_month = add_zero(last_updated_month)
    last_updated_day = add_zero(last_updated_day)

    converted_string = (
        last_updated_year + "-" + last_updated_month + "-" + last_updated_day
    )
    return converted_string
