import psycopg
import requests
import os
from references.last_updated import update_last_updated


pg_db = os.environ["POSTGRES_DB"]
db_host = os.environ["DB_HOST"]
db_user = os.environ["DB_USER"]
db_pass = os.environ["DB_PASS"]
db_port = os.environ["DB_PORT"]
natstat_key = os.environ["NATSTAT_KEY"]

conn = psycopg.connect(
    dbname=pg_db,
    host=db_host,
    user=db_user,
    password=db_pass,
    port=db_port,
)

db = conn.cursor()

db.execute(
    """
    INSERT INTO players(
    player_id,
    full_name,
    school_id
    )
    WITH
    NameCounts AS (
        SELECT
        player_id,
        player_name,
        MAX(team_id) AS team_id,
        COUNT(player_name) AS name_count,
        ROW_NUMBER() OVER (
            PARTITION BY player_id
            ORDER BY COUNT(player_name) DESC)
            AS name_count_rank
        FROM
        player_performances
        WHERE player_id !=0
        GROUP BY 1, 2
    )
    SELECT
    player_id,
    player_name,
    team_id
    FROM NameCounts
    WHERE name_count_rank = 1
    ON CONFLICT (player_id)
    DO UPDATE SET
    full_name = excluded.full_name,
    school_id = excluded.school_id;
    """
)

update_last_updated(db, "players")

conn.commit()
db.close()
conn.close()
