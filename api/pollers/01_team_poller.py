import json
import psycopg
import requests
import os

from references.last_updated import update_last_updated

pg_db = os.environ["POSTGRES_DB"]
db_host = os.environ["DB_HOST"]
db_user = os.environ["DB_USER"]
db_pass = os.environ["DB_PASS"]
db_port = os.environ["DB_PORT"]
natstat_key = os.environ["NATSTAT_KEY"]


conn = psycopg.connect(
    dbname=pg_db,
    host=db_host,
    user=db_user,
    password=db_pass,
    port=db_port,
)
db = conn.cursor()
fetch_url = (
    f"https://api.natstat.com/v2/teams/MBB/"
    f"?key={natstat_key}&format=json&max=1000"
)

res = requests.get(fetch_url)
response = json.loads(res.text)
for team in response["teams"]:
    team_id = response["teams"][team]["ID"]
    team_name = response["teams"][team]["Name"]
    abbrev = response["teams"][team]["Abbrev"]
    active = response["teams"][team]["Active"]
    db.execute(
        """
        INSERT INTO school_teams(
            id,
            name,
            abbrev,
            active
        )
        VALUES (
            %s,
            %s,
            %s,
            %s
        )
        ON CONFLICT (id)
        DO UPDATE SET
        name=%s,
        abbrev=%s,
        active=%s
        """,
        [team_id, team_name, abbrev, active, team_name, abbrev, active],
    )
update_last_updated(db, "school_teams")

conn.commit()
db.close()
conn.close()
print("Completed team poller")
